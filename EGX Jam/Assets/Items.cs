using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Items : MonoBehaviour
{
    //private Rigidbody rigidbody;
    [SerializeField] private Sprite sprite;
    [SerializeField] private string itemName;

    [SerializeField]
    private Rigidbody rigidbody;

    private Basket basket;

    private bool isPickedUp;

    private PlayerPickItems player;
    public Sprite Sprite { get => sprite; set => sprite = value; }
    public string ItemName { get => itemName; set => itemName = value; }
    public bool IsPickedUp { get => isPickedUp; set => isPickedUp = value; }

    private bool isAdded;

    private void Start()
    {
        //rigidbody = GetComponent<Rigidbody>();   
        rigidbody = GetComponentInParent<Rigidbody>();

        isPickedUp = false;
    }

    private void Update()
    {
        if (IsPickedUp)
        {
      
            //transform.position = new Vector3(player.position.x, player.position.y / 2, player.position.z + offset);

            transform.position = player.HoldPosition.position;

            transform.rotation = player.HoldPosition.rotation;

            rigidbody.useGravity = false;
        }
        else
        {
            rigidbody.useGravity = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player = other.GetComponent<PlayerPickItems>();  //WORKS!
        }

        if(other.gameObject.tag == "Basket")
        {

            if(isAdded == false)
            {

                basket = other.GetComponent<Basket>();

                basket.GameObjects.Add(this.gameObject);

                isAdded = true;

            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Basket")
        {
            if(isAdded == true)
            {
                this.gameObject.transform.position = basket.Position1.position;
            }
        }
    }
}
