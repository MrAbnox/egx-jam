﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exclamation : MonoBehaviour
{
    [SerializeField] bool pop;
    bool exclaiming;
    [SerializeField] float exclaimTimer;
    [SerializeField] Vector3 scale;

    // Start is called before the first frame update
    void Start()
    {
        scale = gameObject.transform.localScale;
        exclaiming = false;
        transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (exclaiming == true)
        {
            exclaimTimer += 2.5f * Time.deltaTime;
            transform.localScale = scale * (1.5f * Mathf.Sin(exclaimTimer));
            if (Mathf.Sin(exclaimTimer) < 0)
            {
                exclaiming = false;
                transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            }
        }

        if (pop)
        {
            pop = false;
            Exclaim();
        }

    }

    public void Exclaim()
    {
        exclaiming = true;
        exclaimTimer = 0.0f;
    }
}
