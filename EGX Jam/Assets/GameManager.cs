using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    // Singleton
    private static GameManager instance;

    // vars
    private int score;
    private int currentShopList;


    [SerializeField] private float timer;
    [SerializeField] private float startTime;
    [SerializeField] private float currentTime;
    [SerializeField] private ShopListManager shoppingListManager;
    [SerializeField] private ShoppingListManagerUI shoppingListManagerUI;

    [SerializeField] private List<GameObject>[] shoppingLists = new List<GameObject>[3];

    [SerializeField] private TextMeshProUGUI timerUI;

    [SerializeField] private List<GameObject> shoppingListsObject;
    [SerializeField] private List<GameObject> shoppingListsObjectUI;

    public List<GameObject>[] ShoppingLists { get => shoppingLists; set => shoppingLists = value; }
    public List<GameObject> ShoppingListsObject { get => shoppingListsObject; set => shoppingListsObject = value; }
    public List<GameObject> ShoppingListsObjectUI { get => shoppingListsObjectUI; set => shoppingListsObjectUI = value; }


    // Start is called before the first frame update
    void Start()
    {

        if (instance != null)
        {
            Debug.LogWarning("More then one Game Manager!");
            return;
        }

        currentTime = startTime * 60.0f;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= Time.deltaTime;

        timerUI.text = ((int)(currentTime / 60.0f)).ToString() + ':' + ((int)((currentTime / 60.0f % 1.0f) * 60.0f)).ToString();

        if(currentTime < 0.0f)
        {
            EndGame();
        }
    }

    public void ChangeScore(int change)
    {
        score = Mathf.Clamp(score + change, 0, int.MaxValue);
    }

    public void AddShopList(GameObject recipe)
    {
        shoppingListsObject.Add(recipe);
        ShoppingLists[currentShopList] = recipe.GetComponent<RandomRecipes>().RecipeItems;

        //for(int i = 0; i < ShoppingLists[currentShopList].Count; i++)
        //{
        //    Debug.Log(ShoppingLists[currentShopList][i].name);
        //}

        shoppingListManagerUI.CreateNewList(recipe.GetComponent<RandomRecipes>().RecipeItems);
        currentShopList ++;

    }

    public void RemoveShopList(int shopListNumber)
    {
        shoppingListManager.RemoveOrder();
        Destroy(shoppingListsObjectUI[shopListNumber]);
        shoppingListsObject[shopListNumber].GetComponent<RandomRecipes>().IsRecipeDone = true;
        shoppingListsObject[shopListNumber] = null;
        shoppingLists[shopListNumber].Clear();
        currentShopList--;
    }

    void EndGame()
    {

    }

    public static GameManager GetInstance()
    {
      

        return instance;
    }
}
//=======
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using TMPro;

//public class GameManager : MonoBehaviour
//{
//    // Singleton
//    private static GameManager instance;

//    // vars
//    private int score;
//    private int currentShopList;


//    [SerializeField] private float timer;
//    [SerializeField] private float startTime;
//    [SerializeField] private float currentTime;
//    [SerializeField] private ShoppingListManagerUI shoppingListManagerUI;


//    [SerializeField] private TextMeshProUGUI timerUI;

//    // Start is called before the first frame update
//    void Start()
//    {

//        if (instance != null)
//        {
//            Debug.LogWarning("More then one Game Manager!");
//            return;
//        }

//        currentTime = startTime * 60.0f;
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        currentTime -= Time.deltaTime;

//        timerUI.text = ((int)(currentTime / 60.0f)).ToString() + ':' + ((int)((currentTime / 60.0f % 1.0f) * 60.0f)).ToString();

//        if(currentTime < 0.0f)
//        {
//            EndGame();
//        }
//    }

//    public void ChangeScore(int change)
//    {
//        score = Mathf.Clamp(score + change, 0, int.MaxValue);
//    }

//    public void AddShopList(List<GameObject> listItems)
//    {
//        shoppingListManagerUI.CreateNewList(listItems);
//        currentShopList ++;
//    }

//    public void RemoveShopList()
//    {
//        currentShopList = Mathf.Clamp(currentShopList--, 0, 100);
//    }

//    void EndGame()
//    {

//    }

//    public static GameManager GetInstance()
//    {
      

//        return instance;
//    }
//}
//>>>>>>> master
