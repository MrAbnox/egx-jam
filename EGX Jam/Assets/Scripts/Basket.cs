//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class Basket : MonoBehaviour
//{
//    private bool isPickedUp;

//    private bool doOnce;

//    [SerializeField]
//    private PlayerPickItems player;

//    [SerializeField]
//    private Rigidbody rigidbody;

//    [SerializeField]
//    private float offset = 5.0f;

//    private int currentListObject = 0;

//    [SerializeField]
//    private Transform position1;

//    [SerializeField]
//    private Transform position2;

//    [SerializeField]
//    private Transform position3;

//    [SerializeField]
//    private Transform position4;

//    private GameObject sausages;
//    private GameObject apple;
//    private GameObject chocolate;
//    private GameObject milk;
//    private GameObject bread;
//    private GameObject steak;

//    [SerializeField]
//    private List<GameObject> gameObjects;

//    public bool IsPickedUp { get => isPickedUp; set => isPickedUp = value; }
//    public List<GameObject> GameObjects { get => gameObjects; set => gameObjects = value; }
//    public bool DoOnce { get => doOnce; set => doOnce = value; }

//    // Start is called before the first frame update
//    void Start()
//    {
//        rigidbody = GetComponentInParent<Rigidbody>();
//        IsPickedUp = false;
//    }

//    private void OnTriggerEnter(Collider other)
//    {
//        if(other.gameObject.tag == "Player")
//        {
//            player = other.GetComponent<PlayerPickItems>();  //WORKS!
//        }
//    }
//    // Update is called once per frame
//    void FixedUpdate()
//    {
//        if(IsPickedUp)
//        {

//            //transform.position = new Vector3(player.position.x, player.position.y / 2, player.position.z + offset);

//            transform.position = player.HoldPosition.position;

//            transform.rotation = player.HoldPosition.rotation;

//            rigidbody.useGravity = false;
//        }
//        else
//        {
//            rigidbody.useGravity = true;
//        }
//    }

//    public void Drop()
//    {
//        isPickedUp = false;
//    }

//    public void AddObject(GameObject obj)
//    {
//        if(obj.name == "Sausages")
//        {
//            CheckList(obj);
//        }
//        else if (obj.name == "Bread")
//        {
//            CheckList(obj);
//        }
//        else if (obj.name == "Chocolate")
//        {
//            CheckList(obj);
//        }
//        else if (obj.name == "Milk")
//        {
//            CheckList(obj);
//        }
//        else if (obj.name == "Jam")
//        {
//            CheckList(obj);
//        }
//        else if (obj.name == "Steak")
//        {
//            CheckList(obj);
//        }
//        else if (obj.name == "Apple")
//        {
//            CheckList(obj);
//        }
//    }

//    void CheckList(GameObject obj)
//    {
//        if(doOnce == false)
//        {

//            gameObjects.Add(obj);
//            doOnce = true;
//        }

//        if(gameObjects.Count == 1)
//        {
//            obj.transform.position = position1.transform.position;
//            obj.transform.rotation = position1.transform.rotation;
//        }
//        else if (gameObjects.Count == 2)
//        {
//            obj.transform.position = position2.transform.position;
//            obj.transform.rotation = position2.transform.rotation;
//        }
//        else if (gameObjects.Count == 3)
//        {
//            obj.transform.position = position3.transform.position;
//            obj.transform.rotation = position3.transform.rotation;
//        }
//        else if (gameObjects.Count == 4)
//        {
//            obj.transform.position = position4.transform.position;
//            obj.transform.rotation = position4.transform.rotation;
//        }
//    }
//}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Basket : MonoBehaviour
{
    private bool isPickedUp;

    private bool doOnce;

    private int currentItem = 0;

    [SerializeField]
    private PlayerPickItems player;

    [SerializeField]
    private Rigidbody rigidbody;

    [SerializeField]
    private float offset = 5.0f;

    private int currentListObject = 0;

    [SerializeField]
    private Transform position1;

    [SerializeField]
    private Transform position2;

    [SerializeField]
    private Transform position3;

    [SerializeField]
    private Transform position4;

    private GameObject objectOne;
    private GameObject objectTwo;
    private GameObject objectThree;
    private GameObject objectFour;

    public UnityEvent unityE;

    [SerializeField]
    private List<GameObject> gameObjects;

    public bool IsPickedUp { get => isPickedUp; set => isPickedUp = value; }
    public List<GameObject> GameObjects { get => gameObjects; set => gameObjects = value; }
    public bool DoOnce { get => doOnce; set => doOnce = value; }
    public Transform Position1 { get => position1; set => position1 = value; }
    public Transform Position2 { get => position2; set => position2 = value; }
    public Transform Position3 { get => position3; set => position3 = value; }
    public Transform Position4 { get => position4; set => position4 = value; }


    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponentInParent<Rigidbody>();
        IsPickedUp = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            player = other.GetComponent<PlayerPickItems>();  //WORKS!
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        for (int i = 0; i < gameObjects.Count; i++)
        {
            if (i == 0)
            {
                if(objectOne == true)
                {

                    objectOne.transform.position = Position1.transform.position;

                   Debug.Log("TEST!");
                }
            }
            if (i == 1)
            {
                gameObjects[i].transform.position = Position2.transform.position;
                Debug.Log("TEST2!");
            }
            if (i == 2)
            {
                GameObjects[i].transform.position = Position3.transform.position;
            }
            if (i == 3)
            {
                GameObjects[i].transform.position = Position4.transform.position;
            }
        }

        if (IsPickedUp)
        {
      
            //transform.position = new Vector3(player.position.x, player.position.y / 2, player.position.z + offset);

            transform.position = player.HoldPosition.position;

            transform.rotation = player.HoldPosition.rotation;

            rigidbody.useGravity = false;
        }
        else
        {
            rigidbody.useGravity = true;
        }
    }

    public void Drop()
    {
        isPickedUp = false;
    }

    public void AddObject(GameObject obj)
    {

            
        gameObjects.Add(obj);

        currentItem = gameObjects.Count;

        switch (currentItem)
        {
            case 1:

                objectOne = obj;

                Debug.Log("try out this");

                break;

            case 2:

                objectTwo = obj;
               

                break;

            case 3:

                objectThree = obj;

                break;

            case 4:

                objectFour = obj;

                break;
        }


            objectOne = obj;
            doOnce = true;
        
    }
}