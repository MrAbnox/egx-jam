﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float horizontalMove;
    float verticalMove;

    [SerializeField] bool isPlayer2;
    [SerializeField] float stunDuration;
    [SerializeField] GameObject[] stars;
    [SerializeField] float maxStamina;
    [SerializeField] float stamina;

    bool isSprinting;
    public float Speed;
    public float sprintingSpeed;
    public float RotateSpeed;

    bool isStunned;

    [SerializeField] private Vector3 velocity;
    [SerializeField] GameObject trail;
    private Vector3 direction;

    [SerializeField]
    private CharacterController playerController;

    // Start is called before the first frame update
    void Start()
    {
        direction = new Vector3(1.0f, 0.0f, 0.0f);
        trail.GetComponent<ParticleSystem>().Stop();
        isSprinting = false;
        stamina = maxStamina;
        for(int a = 0; a < stars.Length; a++)
        {
            stars[a].SetActive(false);
        }
        isStunned = false;
        velocity = new Vector3(0.0f, 0.0f, 0.0f);
        playerController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMove();
    }

    void PlayerMove()
    {
        Vector3 movement = new Vector3 (0.0f, 0.0f, 0.0f);
        if(isPlayer2)
        {
            if (Input.GetKey(KeyCode.C) && stamina > 3.0f && !isSprinting)
            {
                isSprinting = true;
                trail.GetComponent<ParticleSystem>().Play();
            }
            if (Input.GetKeyUp(KeyCode.C))
            {
                isSprinting = false;
                trail.GetComponent<ParticleSystem>().Stop();
            }
            if (Input.GetKey(KeyCode.W))
            {
                movement.z += 1.0f;
            }
            if (Input.GetKey(KeyCode.S))
            {
                movement.z -= 1.0f;
            }
            if (Input.GetKey(KeyCode.A))
            {
                movement.x -= 1.0f;
            }
            if (Input.GetKey(KeyCode.D))
            {
                movement.x += 1.0f;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.J) && stamina > 3.0f && !isSprinting)
            {
                isSprinting = true;
                trail.GetComponent<ParticleSystem>().Play();
            }
            if (Input.GetKeyUp(KeyCode.J))
            {
                isSprinting = false;
                trail.GetComponent<ParticleSystem>().Stop();
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                movement.z += 1.0f;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                movement.z -= 1.0f;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                movement.x -= 1.0f;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                movement.x += 1.0f;
            }
        }


        if(isSprinting)
        {
            stamina -= 2.5f * Time.deltaTime;
        }
        else
        {
            stamina += Time.deltaTime;
        }

        if(stamina > maxStamina)
        {
            stamina = maxStamina;
        }

        if(stamina < 0.0f)
        {
            isSprinting = false;
            trail.GetComponent<ParticleSystem>().Stop();
        }

        if(!isStunned)
        {
            if (movement.x == 0.0f && movement.y == 0.0f && movement.z == 0.0f)
            {
                //Nothing lol
            }
            else
            {
                direction = movement;
            }

            movement = movement.normalized;
            if(isSprinting)
            {
                velocity += movement * sprintingSpeed * Time.deltaTime;
            }
            else
            {
                velocity += movement * Speed * Time.deltaTime;
            }
        }

        Quaternion lookOnLook =
        Quaternion.LookRotation(direction);

        transform.rotation =
        Quaternion.RotateTowards(transform.rotation, lookOnLook, Time.deltaTime * 300.0f);



        velocity = velocity * 0.97f;

        playerController.SimpleMove(velocity);

    }    

    public void Stun(Vector3 guard)
    {
        velocity = (gameObject.transform.position - guard).normalized * 12.0f ;
        StartCoroutine(StunRoutine());
    }

    public IEnumerator StunRoutine()
    {
        isStunned = true;
        for (int a = 0; a < stars.Length; a++)
        {
            stars[a].SetActive(true);
        }
        yield return new WaitForSeconds(stunDuration);
        isStunned = false;
        for (int a = 0; a < stars.Length; a++)
        {
            stars[a].SetActive(false);
        }
    }
}