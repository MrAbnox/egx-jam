﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickItems : MonoBehaviour
{
    private Basket basket;

    private Items item;

    private bool isHoldingBasket;

    private bool isHoldingItem;

    [SerializeField]
    private GameObject ChocolateBar;

    [SerializeField]
    private GameObject Apple;

    [SerializeField]
    private GameObject Bread;

    [SerializeField]
    private GameObject Milk;

    [SerializeField]
    private GameObject Jam;

    [SerializeField]
    private GameObject Steak;

    [SerializeField]
    private GameObject Sausages;

    [SerializeField]
    private Transform holdPosition;

    public Transform HoldPosition { get => holdPosition; set => holdPosition = value; }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Item")
        {
            //if the player press e while the ray is getting casted to the object
            if (Input.GetKeyDown(KeyCode.E))
            {
                //add to basket
                Destroy(other.gameObject);
            }

            //check if item has been pick (TO DO)
            if(other.gameObject.GetComponent<Items>().IsPickedUp == false)
            {


                if (Input.GetKeyDown(KeyCode.E))
                {

                    item = other.gameObject.GetComponent<Items>();
                    item.IsPickedUp = true;
                    isHoldingItem = true;
                }
                //Destroy(other.gameObject);
            }
        
            //if the player press e while the ray is getting casted to the object

        }
        else if(other.gameObject.tag == "Basket")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                basket = other.GetComponent<Basket>();
                basket.IsPickedUp = true;
            }
        }

        if(isHoldingItem == true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                //item.drop();
                item.IsPickedUp = false;
                isHoldingItem = false;
            }
        }
    }
}
