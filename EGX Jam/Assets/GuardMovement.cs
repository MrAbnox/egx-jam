﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GuardMovement : MonoBehaviour
{
    enum State { PATROLLING, CHASING, WALKING, STANDING }

    [SerializeField]private float movementSpeed;
    [SerializeField] private float chasingSpeed;
    [SerializeField] private float chaseCooldown;
    [SerializeField] private float wayPointDetectionRadius;
    [SerializeField] private float playerDetectionRadius;
    [SerializeField] private float playerHearingRadius;
    [SerializeField] private float playerDetectionAngle;

    [SerializeField] private float wayPointVariance;
    [SerializeField] private float visitingPenalty;
    [SerializeField] private int wayPointCounter;

    [SerializeField] private Transform player1;
    [SerializeField] private Transform player2;
    [SerializeField] private Transform[] wayPoints;
    [SerializeField] private float[] wayPointValues;
    [SerializeField] private float[] wayPointBaseValues;

    [SerializeField] private float persistanceTimer;
    [SerializeField] GameObject exclamationMark;
    [SerializeField] GameObject camera;

    private int target;
 
    State state;

    NavMeshAgent navMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        navMeshAgent.speed = movementSpeed;
        wayPointCounter = 0;
        navMeshAgent.SetDestination(wayPoints[wayPointCounter].position);
        FindNewWayPoint();

        state = State.PATROLLING;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.PATROLLING || state == State.WALKING)
        {
            if ((gameObject.transform.position - wayPoints[wayPointCounter].position).magnitude < wayPointDetectionRadius)
            {
                FindNewWayPoint();
            }
        }

        if (state == State.PATROLLING)
        {
            if ((gameObject.transform.position - player1.position).magnitude < playerDetectionRadius)
            {
                float angle = Vector3.Angle(player1.position - gameObject.transform.position, transform.forward);
                if (angle < playerDetectionAngle)
                {
                    LayerMask mask = LayerMask.GetMask("Wall");
                    if (!Physics.Raycast(transform.position, player1.position - transform.position, 20.0f, mask))
                    {
                        exclamationMark.GetComponent <Exclamation> (). Exclaim();
                        target = 1;
                        StartCoroutine(Chase());
                    }
                }
                else
                {
                    if ((gameObject.transform.position - player1.position).magnitude < playerHearingRadius)
                    {
                        exclamationMark.GetComponent<Exclamation>().Exclaim();
                        target = 1;
                        StartCoroutine(Chase());
                    }
                }
            }

            if ((gameObject.transform.position - player2.position).magnitude < playerDetectionRadius)
            {
                float angle = Vector3.Angle(player2.position - gameObject.transform.position, transform.forward);
                if (angle < playerDetectionAngle)
                {
                    LayerMask mask = LayerMask.GetMask("Wall");
                    if (!Physics.Raycast(transform.position, player2.position - transform.position, 20.0f, mask))
                    {
                        exclamationMark.GetComponent<Exclamation>().Exclaim();
                        target = 2;
                        StartCoroutine(Chase());
                    }
                }
                else
                {
                    if ((gameObject.transform.position - player2.position).magnitude < playerHearingRadius)
                    {
                        exclamationMark.GetComponent<Exclamation>().Exclaim();
                        target = 2;
                        StartCoroutine(Chase());
                    }
                }
            }
        }

        if(state == State.CHASING)
        {
            if(target == 1)
            {
                navMeshAgent.SetDestination(player1.position);
                if ((gameObject.transform.position - player1.position).magnitude < 2.5f)
                {
                    player1.GetComponent<PlayerMovement>().Stun(gameObject.transform.position);
                    camera.GetComponent<Camera>().Shake();
                    StopChasing();
                }

                if ((gameObject.transform.position - player1.position).magnitude < playerDetectionRadius)
                {
                    float angle = Vector3.Angle(player1.position - gameObject.transform.position, transform.forward);
                    if (angle < playerDetectionAngle)
                    {
                        LayerMask mask = LayerMask.GetMask("Wall");
                        if (!Physics.Raycast(transform.position, player1.position - transform.position, 20.0f, mask))
                        {
                            persistanceTimer = 1.0f;
                        }
                    }
                }
                else
                {
                    persistanceTimer -= Time.deltaTime;
                }

                if (persistanceTimer < 0)
                {
                    StopChasing();
                }
            }
            else
            {
                navMeshAgent.SetDestination(player2.position);
                if ((gameObject.transform.position - player2.position).magnitude < 2.5f)
                {
                    player2.GetComponent<PlayerMovement>().Stun(gameObject.transform.position);
                    StopChasing();
                    camera.GetComponent<Camera>().Shake();
                }

                if ((gameObject.transform.position - player2.position).magnitude < playerDetectionRadius)
                {
                    float angle = Vector3.Angle(player2.position - gameObject.transform.position, transform.forward);
                    if (angle < playerDetectionAngle)
                    {
                        LayerMask mask = LayerMask.GetMask("Wall");
                        if (!Physics.Raycast(transform.position, player2.position - transform.position, 20.0f, mask))
                        {
                            persistanceTimer = 1.0f;
                        }
                    }
                }
                else
                {
                    persistanceTimer -= Time.deltaTime;
                }

                if (persistanceTimer < 0)
                {
                    StopChasing();
                }
            }

        }
    }

    void FindNewWayPoint()
    {
        wayPointBaseValues[wayPointCounter] -= visitingPenalty; 

        for (int a = 0; a < wayPoints.Length; a++)
        {
            wayPointBaseValues[a] = wayPointBaseValues[a] * 0.9f;
            wayPointValues[a] = wayPointBaseValues[a];
            wayPointValues[a] -= (gameObject.transform.position - wayPoints[a].position).magnitude;
            wayPointValues[a] += Random.Range(wayPointVariance, 10.0f);
        }
        wayPointValues[wayPointCounter] -= 100000.0f;

        float HighestValue = wayPointValues[0];
        int HighestWayPoint = 0;
        for (int a = 1; a < wayPoints.Length; a++)
        {
            if (wayPointValues[a] > HighestValue)
            {
                HighestValue = wayPointValues[a];
                HighestWayPoint = a;
            }
        }
        wayPointCounter = HighestWayPoint;
        navMeshAgent.SetDestination(wayPoints[HighestWayPoint].position);
    }



    void StopChasing()
    {
        StartCoroutine(ChaseCooldown());
        navMeshAgent.speed = movementSpeed;
        FindNewWayPoint();
    }

    IEnumerator ChaseCooldown()
    {
        state = State.WALKING;
        yield return new WaitForSeconds(chaseCooldown);
        state = State.PATROLLING;
    }

    IEnumerator Chase()
    {
        state = State.STANDING;
        navMeshAgent.speed = 0.0f;
        yield return new WaitForSeconds(0.8f);
        state = State.CHASING;
        if (target == 1)
        {
            navMeshAgent.SetDestination(player1.position);
        }
        else
        {
            navMeshAgent.SetDestination(player2.position);
        }
        navMeshAgent.speed = chasingSpeed;
    }

}
