
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRecipes : MonoBehaviour
{

    private int itemsNumber;
    private float recipeTime;
    private float currentTime;

    static float TIME_PER_ITEM = 10.0f;
    static int SCORE_PER_ITEM = 10;

    private int maxItems = 2;
    private int minItems = 1;

    [SerializeField] private int score;
    [SerializeField] private GameObject[] items;
    [SerializeField] private List<GameObject> recipeItems;

    private GameManager gameManager;

    private bool isRecipeDone;

    public List<GameObject> RecipeItems { get => recipeItems; set => recipeItems = value; }
    public bool IsRecipeDone { get => isRecipeDone; set => isRecipeDone = value; }

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        itemsNumber = 0;
        IsRecipeDone = false;

        itemsNumber = Random.Range(minItems, maxItems);
        recipeTime = itemsNumber * TIME_PER_ITEM;

        score = itemsNumber * SCORE_PER_ITEM;

        for (int i = 0; i < itemsNumber; i++)
        {
            int j = Random.Range(0, items.Length);

            RecipeItems.Add(items[j]);
        }

        gameManager.AddShopList(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(!IsRecipeDone)
        {
            currentTime += Time.deltaTime;

            if (currentTime > recipeTime)
            {
                //IsRecipeDone = true;
            }
        }
        else
        {
            // TODO ADD TO SCORE
            Debug.Log("DONE!");
           Destroy(gameObject);
        }
    }
}
//=======
//<<<<<<< HEAD
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class RandomRecipes : MonoBehaviour
//{


//    private int itemsNumber;
//    private float recipeTime;
//    private float currentTime;

//    static float TIME_PER_ITEM = 10.0f;

//    private int maxItems = 5;
//    private int minItems = 1;

//    [SerializeField] private int score;
//    [SerializeField] private GameObject[] items;
//    [SerializeField] private List<GameObject> recipeItems;

//    private GameManager gameManager;

//    private bool isRecipeDone;

//    // Start is called before the first frame update
//    void Start()
//    {
//        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

//        itemsNumber = 0;
//        isRecipeDone = false;

//        itemsNumber = Random.Range(minItems, maxItems);
//        recipeTime = itemsNumber * TIME_PER_ITEM;

//        for (int i = 0; i < itemsNumber; i++)
//        {
//            int j = Random.Range(0, items.Length);

//            recipeItems.Add(items[j]);
//        }

//        gameManager.AddShopList(recipeItems);
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if(!isRecipeDone)
//        {
//            currentTime += Time.deltaTime;

//            if (currentTime > recipeTime)
//            {
//                isRecipeDone = true;
//            }
//        }
//        else
//        {
//            // TODO ADD TO SCORE
//            //GameManager.instance.RemoveShopList();
//           // Destroy(gameObject);
//        }
//    }
//}
//=======
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class RandomRecipes : MonoBehaviour
//{


//    private int itemsNumber;
//    private float recipeTime;
//    private float currentTime;

//    static float TIME_PER_ITEM = 10.0f;

//    private int maxItems = 5;
//    private int minItems = 1;

//    [SerializeField] private int score;
//    [SerializeField] private GameObject[] items;
//    [SerializeField] private List<GameObject> recipeItems;

//    private GameManager gameManager;

//    private bool isRecipeDone;

//    // Start is called before the first frame update
//    void Start()
//    {
//        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

//        itemsNumber = 0;
//        isRecipeDone = false;

//        itemsNumber = Random.Range(minItems, maxItems);
//        recipeTime = itemsNumber * TIME_PER_ITEM;

//        for (int i = 0; i < itemsNumber; i++)
//        {
//            int j = Random.Range(0, items.Length);

//            recipeItems.Add(items[j]);
//        }

//        gameManager.AddShopList(recipeItems);
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if(!isRecipeDone)
//        {
//            currentTime += Time.deltaTime;

//            if (currentTime > recipeTime)
//            {
//                isRecipeDone = true;
//            }
//        }
//        else
//        {
//            // TODO ADD TO SCORE
//            //GameManager.instance.RemoveShopList();
//           // Destroy(gameObject);
//        }
//    }
//}


