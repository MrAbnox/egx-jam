using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManagerUI : MonoBehaviour
{

    [SerializeField] private GameObject item;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateItems(List<GameObject> listItems)
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            GameObject obj = Instantiate(item);
            obj.transform.parent = transform;

            obj.GetComponent<ItemUI>().ItemSprite.sprite = listItems[i].GetComponent<Items>().Sprite;
            obj.GetComponent<ItemUI>().TextName.text = listItems[i].GetComponent<Items>().ItemName;

        }

        

    }
}
//=======
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ItemManagerUI : MonoBehaviour
//{

//    [SerializeField] private GameObject item;
//    // Start is called before the first frame update
//    void Start()
//    {
       
//    }

//    // Update is called once per frame
//    void Update()
//    {
        
//    }

//    public void CreateItems(List<GameObject> listItems)
//    {
//        for (int i = 0; i < listItems.Count; i++)
//        {
//            GameObject obj = Instantiate(item);
//            obj.transform.parent = transform;

//            obj.GetComponent<ItemUI>().ItemSprite.sprite = listItems[i].GetComponent<Items>().Sprite;
//            obj.GetComponent<ItemUI>().TextName.text = listItems[i].GetComponent<Items>().ItemName;

//        }

        

//    }
//}
//>>>>>>> master
