﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] private Transform player1;
    [SerializeField] private Transform player2;

    bool isShaking;
    float shakeTimer;

    private Transform centrePoint;

    float Zoom;

    // Start is called before the first frame update
    void Start()
    {
        isShaking = false;
        centrePoint = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        float Distance = (player1.position - player2.position).magnitude;

        Zoom = 0.0f;

        centrePoint.position = player1.position + 0.5f * (player2.position - player1.position);
        float xDif = 0.6f * (25.0f + Mathf.Abs(player2.position.x - player1.position.x));
        float zDif = 0.9f * (25.0f + Mathf.Abs(player2.position.z - player1.position.z));



        if (xDif > zDif)
        {
            Zoom += xDif;
        }
        else
        {
            Zoom += zDif;
        }

        centrePoint.position = new Vector3(centrePoint.position.x, centrePoint.position.y - (transform.forward.y * Zoom), centrePoint.position.z - (transform.forward.z * Zoom));
        gameObject.transform.position = centrePoint.position;

        if (isShaking)
        {
            shakeTimer -= Time.deltaTime;
            gameObject.transform.position = new Vector3(transform.position.x + Mathf.Sin(Mathf.Deg2Rad * (18000.0f * shakeTimer)) * 5.0f *shakeTimer, transform.position.y, transform.position.z);
            if(shakeTimer < 0.0f)
            {
                isShaking = false;
            }
        }

    }

    public void Shake()
    {
        isShaking = true;
        shakeTimer = 0.2f;
    }
}
