﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    private Vector3 centre;
    [SerializeField] private float angle;
    [SerializeField] private float radius;
    [SerializeField] private float bobHeight;
    [SerializeField] private Vector3 newPosition;


    // Start is called before the first frame update
    void Start()
    {
        centre = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        angle += 2.0f * Time.deltaTime;
        newPosition = new Vector3(centre.x + (radius * Mathf.Cos(angle)), centre.y + bobHeight * Mathf.Cos(angle * 3), centre.x + (radius * Mathf.Sin(angle)));
        transform.localPosition = newPosition;
        Vector3 LookPosition = new Vector3(transform.position.x, transform.position.y - 5.0f, transform.position.z + 5.0f);
        transform.LookAt(LookPosition);
    }
}
