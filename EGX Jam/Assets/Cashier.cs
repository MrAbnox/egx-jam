﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cashier : MonoBehaviour
{
    bool IsChecking = false;
    [SerializeField] private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Basket")
        {
            Basket basket = other.gameObject.GetComponent<Basket>();
            if (!basket.IsPickedUp)
            {
                if (!IsChecking)
                {
                    CheckLists(basket);
                    Destroy(other);
                }
            }
        }
    }

    void CheckLists(Basket basket)
    {
        IsChecking = true;

        for(int i = 0; i < gameManager.ShoppingLists.Length; i++)
        {
            if (gameManager.ShoppingLists[i] != null)
            {
                if (gameManager.ShoppingLists[i].Count == basket.GameObjects.Count)
                {

                    int itemsCounts = gameManager.ShoppingLists[i].Count;
                    int itemsMatched = 0;

                    for (int j = 0; j < gameManager.ShoppingLists[i].Count; j++)
                    {
                        for (int k = 0; k < gameManager.ShoppingLists[i].Count; k++)
                        {
                            if (gameManager.ShoppingLists[i][j].GetComponent<Items>().ItemName == basket.GameObjects[k].GetComponent<Items>().ItemName)
                            {
                                itemsMatched++;
                                Debug.Log(gameManager.ShoppingLists[i][j].GetComponent<Items>().ItemName + " Matched!");
                            }
                        }
                    }

                    if (itemsMatched == itemsCounts)
                    {
                        Debug.Log("worked!");
                        AddScore(i);
                    }
                }
            }
        }
      
        IsChecking = false;
    }

    void AddScore(int shopListNumber)
    {
        gameManager.RemoveShopList(shopListNumber);
    }
}
