using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopListManager : MonoBehaviour
{
    private int currentOrders;
    [SerializeField] private int maxOrders;
    [SerializeField] private GameObject randomShoplist;

    [SerializeField] private float minTime;
    [SerializeField] private float maxTime;

    bool IsAdding;

    // Start is called before the first frame update
    void Start()
    {
        IsAdding = false;
        currentOrders++;
        Instantiate(randomShoplist);
    }

    // Update is called once per frame
    void Update()
    {
        if(currentOrders < maxOrders)
        {
            if (!IsAdding)
            {
                StartCoroutine(AddShopList());
            }
        }
    }

   

    IEnumerator AddShopList()
    {
        IsAdding = true;
        currentOrders++;
        float randomTime = Random.Range(minTime, maxTime);
        yield return new WaitForSeconds(randomTime);
        Instantiate(randomShoplist);
        IsAdding = false;
    }

    public void RemoveOrder()
    {
        currentOrders--;
    }
}
//=======
//<<<<<<< HEAD
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ShopListManager : MonoBehaviour
//{
//    private int currentOrders;
//    [SerializeField] private int maxOrders;
//    [SerializeField] private GameObject randomShoplist;

//    // Start is called before the first frame update
//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if(currentOrders < maxOrders)
//        {
//            AddShopList();
//        }
//    }

//   void AddShopList()
//    {
//        currentOrders++;
//        Instantiate(randomShoplist);
//    }
//}
//=======
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ShopListManager : MonoBehaviour
//{
//    private int currentOrders;
//    [SerializeField] private int maxOrders;
//    [SerializeField] private GameObject randomShoplist;

//    // Start is called before the first frame update
//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if(currentOrders < maxOrders)
//        {
//            AddShopList();
//        }
//    }

//   void AddShopList()
//    {
//        currentOrders++;
//        Instantiate(randomShoplist);
//    }
//}
//>>>>>>> master
//>>>>>>> master
