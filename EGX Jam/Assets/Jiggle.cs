﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jiggle : MonoBehaviour
{
    [SerializeField] bool pop;
    bool popping;
    [SerializeField] float poppingTimer;
    [SerializeField] Vector3 scale;

    // Start is called before the first frame update
    void Start()
    {
        scale = gameObject.transform.localScale;
        popping = false;   
    }

    // Update is called once per frame
    void Update()
    {
        if (popping == true)
        {
            poppingTimer += 15 * Time.deltaTime;
            transform.localScale = scale * (1.0f + (0.5f * Mathf.Sin(poppingTimer)));
            if (Mathf.Sin(poppingTimer) < 0 )
            {
                popping = false;
            }
        }

        if (pop)
        {
            pop = false;
            Pop();
        }
    }

    void Pop()
    {
        popping = true;
        poppingTimer = 0.0f;
    }
}
