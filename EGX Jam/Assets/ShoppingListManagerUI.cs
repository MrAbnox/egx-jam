using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoppingListManagerUI : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameManager gameManager;
    [SerializeField] private GameObject shoppingList;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateNewList(List<GameObject> listItems)
    {
        GameObject obj = Instantiate(shoppingList);
        obj.transform.parent = transform;

        gameManager.ShoppingListsObjectUI.Add(obj);

        obj.GetComponentInChildren<ItemManagerUI>().CreateItems(listItems);
    }
}
//=======
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ShoppingListManagerUI : MonoBehaviour
//{
//    // Start is called before the first frame update

//    [SerializeField] private GameObject shoppingList;

//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
        
//    }

//    public void CreateNewList(List<GameObject> listItems)
//    {
//        GameObject obj = Instantiate(shoppingList);
//        obj.transform.parent = transform;

//        obj.GetComponentInChildren<ItemManagerUI>().CreateItems(listItems);
//    }
//}
//>>>>>>> master
