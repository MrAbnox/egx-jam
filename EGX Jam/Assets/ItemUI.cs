using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemUI : MonoBehaviour
{

    [SerializeField] private Image itemSprite;
    [SerializeField] private TextMeshProUGUI textName;

    public Image ItemSprite { get => itemSprite; set => itemSprite = value; }
    public TextMeshProUGUI TextName { get => textName; set => textName = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
//=======
//﻿using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using TMPro;

//public class ItemUI : MonoBehaviour
//{

//    [SerializeField] private Image itemSprite;
//    [SerializeField] private TextMeshProUGUI textName;

//    public Image ItemSprite { get => itemSprite; set => itemSprite = value; }
//    public TextMeshProUGUI TextName { get => textName; set => textName = value; }

//    // Start is called before the first frame update
//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
        
//    }
//}
//>>>>>>> master
